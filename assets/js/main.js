$(".btn-menu").click(function () {
  $("body").toggleClass("open-menu");
});

if ($(".btn-top").length > 0) {
  $(window).scroll(function () {
    var e = $(window).scrollTop();
    if (e > 300) {
      $(".btn-top").show()
    } else {
      $(".btn-top").hide()
    }
  });
  $(".btn-top").click(function () {
    $('body,html').animate({
      scrollTop: 0
    })
  });
}

if ($(".nav-style").length > 0) {
  $(window).scroll(function () {
    var e = $(window).scrollTop();
    if (e > 10) {
      $(".main-nav").removeClass("nav-style");
    } else {
      $(".main-nav").addClass("nav-style");
    }
  });
  $(".btn-top").click(function () {
    $('body,html').animate({
      scrollTop: 0
    })
  });
}